using System;
using System.Text;

namespace reemplazamiento
{
    internal class BoyerMoore
    {
        private readonly int numeroCaracteres;
        private int[] caracterMalo;
        private string palabra;

        public BoyerMoore(string palabra)
        {
            this.numeroCaracteres = 256;
            this.palabra = palabra;

            caracterMalo = new int[numeroCaracteres];

            for (int c = 0; c < numeroCaracteres; c++)
                caracterMalo[c] = -1;

            for (int j = 0; j < palabra.Length; j++)
                caracterMalo[palabra[j]] = j;
        }

        public void Buscar(string texto)
        {
            int longitudPalabra = palabra.Length, longitudTexto = texto.Length, ignorar = 0;
            char opcion;

            for (int i = 0; i <= longitudTexto - longitudPalabra; i += ignorar)
            {
                ignorar = 0;
                
                for (int j = longitudPalabra - 1; j >= 0; j--)
                    if (palabra[j] != texto[i + j])
                    {
                        ignorar = Math.Max(1, j - caracterMalo[texto[i + j]]);
                        break;
                    }

                if (ignorar == 0)
                {
                    Console.Write($"Se encontraron coincidencias en la posicion: {i}, reemplazar palabra? (s/n): ");
                    opcion = Char.Parse(Console.ReadLine());

                    if (opcion == 's')
                    {
                        var builderTexto = new StringBuilder(texto);
                        builderTexto.Remove(i, longitudPalabra);
                        builderTexto.Insert(i, "pelicula");
                        texto = builderTexto.ToString();
                    }

                    ignorar++;
                }
            }

            Console.WriteLine($"\n{texto}\n");
        }
    }
}