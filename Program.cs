﻿using System;

namespace boyermoore
{
    class Program
    {
        static void Main(string[] args)
        {
            string palabra = "video";
            string texto = "El video proporciona una manera eficaz para ayudarle a demostrar el punto. Cuando haga clic en Vídeo en línea, puede pegar el código para insertar del video que desea agregar. También puede escribir una palabra clave para buscar en línea el video que mejor se adapte a su documento.";

            var algoritmo = new BoyerMoore(palabra);

            algoritmo.Buscar(texto);
        }
    }
}
